# Register machine interpreter #

This is a Python interpreter for a simple register machine structure, using 
the following instructions set:

- `incr` : adds one to the accumulator.
- `decr` : removes one from the accumulator.
- `read` : reads from standard input.
- `write` : writes to standard output.
- `load` : loads a literal or register value into the accumulator.
- `store` : stores the accumulator value into a register.
- `jump` : moves somewhere else in the program.
- `jz` : `jump` if the accumulator is zero.
- `stop` : end the program.

`load` and `store` can take register addresses such as `2`, or pointers such as
`(2)` (in which case the accumulator goes to whichever register address is 
stored in `2`). `load` can also take literals prefixed with a `#` sign: `#2`.

Write your machine to a file, and run as:

    ./reg_interpreter.py machines/multiply2.reg
    
Use one instruction per line. Instruction references for use in `jump` and `jz`
start at zero (first line). No room for blank or comment lines.
    
Prompts from `read` will start with `> `. You can use the `-d` and `-v` switches 
to get a more verbose output. `-d` will also wait for a confirmation before each 
instruction.
