#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

import machine as rm
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Interpreter for basic load/store register machines.")

    parser.add_argument("machine_file", nargs=1,
                        help="Machine file to be interpreted.")
    parser.add_argument("-d", dest="debug", action='store_true',
                        help="Go step by step, reporting at every step.")
    parser.add_argument("-v", dest="verbose", action='store_true',
                        help="Print each instruction before it is executed.")

    args = parser.parse_args()
    machine = rm.RegisterMachine(args.machine_file[0], args.verbose, args.debug)
    machine.run()

    if machine.status == rm.RAM_STATUS_ERROR:
        print("An error occured while executing the machine:\n%s." % (
            machine.strerror()))
