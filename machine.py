# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with
# this program. If not, see <http://www.gnu.org/licenses/>.

import io

RAM_STATUS_READY = 0
RAM_STATUS_RUNNING = 1
RAM_STATUS_ERROR = 2
RAM_STATUS_STOP = 3

RAM_ERROR_NONE = 0
RAM_ERROR_EMPTY = 1
RAM_ERROR_UNKNOWN = 2
RAM_ERROR_NAN = 3
RAM_ERROR_ADDR = 4
RAM_ERROR_ARGS = 5
RAM_ERROR_TARGET = 6
RAM_ERROR_INPUT = 7


class RegisterMachine:
    def __init__(self, filename, verbose=False, debug=False):
        self.stream = io.open(filename)
        self.acc = 0
        self.regs = {}
        self.instructions = {}
        self.current = 0
        self.status = RAM_STATUS_ERROR
        self.error = RAM_ERROR_NONE
        self.verbose = verbose
        self.debug = debug

        line_n = 0
        for line in self.stream:
            line = line.strip()

            if len(line) <= 0:
                self.error = RAM_ERROR_EMPTY
                return

            self.instructions[line_n] = line.split(' ')
            line_n += 1

        self.status = RAM_STATUS_READY

    def resolve_addr(self, addr):
        addr = str(addr)
        if len(addr) > 2 and addr[0] == '(' and addr[-1] == ')' and \
           addr[1:-1].isdigit():
            addr = self.resolve_addr(addr[1:-1])
            return self.regs[int(addr)]

        elif not addr.isdigit():
            return -RAM_ERROR_ADDR

        if not int(addr) in self.regs:
            self.regs[int(addr)] = 0
        return int(addr)

    def incr(self):
        self.acc += 1
        return RAM_ERROR_NONE

    def decr(self):
        if self.acc > 0:
            self.acc -= 1
        return RAM_ERROR_NONE

    def read(self):
        ipt = str(input("> "))
        if not ipt.isdigit() or int(ipt) < 0:
            return -RAM_ERROR_INPUT
        self.acc = int(ipt)
        return RAM_ERROR_NONE

    def write(self):
        print("%d" % self.acc)
        return RAM_ERROR_NONE

    def load(self):
        args = self.instructions[self.current]
        if len(args) != 2:
            return -RAM_ERROR_ARGS

        source = str(args[1])
        addr = self.resolve_addr(source)

        if source[0] == '#':
            if len(source[0]) < 2 or not str(source[1:]).isdigit():
                return -RAM_ERROR_NAN
            self.acc = int(source[1:])
        elif addr >= 0:
            self.acc = self.regs[int(addr)]
        else:
            return addr

        return RAM_ERROR_NONE

    def store(self):
        args = self.instructions[self.current]
        if len(args) != 2:
            return -RAM_ERROR_ARGS

        source = str(args[1])
        addr = self.resolve_addr(source)
        self.regs[addr] = self.acc
        return RAM_ERROR_NONE

    def jump(self):
        args = self.instructions[self.current]
        if len(args) != 2:
            return -RAM_ERROR_ARGS

        if not str(args[1]).isdigit() or not int(args[1]) in self.instructions:
            return -RAM_ERROR_TARGET

        self.current = int(args[1])
        return self.current

    def jz(self):
        if self.acc == 0:
            return self.jump()
        return RAM_ERROR_NONE

    def stop(self):
        self.status = RAM_STATUS_STOP
        return RAM_ERROR_NONE

    def step(self):
        if self.status not in (RAM_STATUS_READY, RAM_STATUS_RUNNING):
            return

        # Rules for these methods:
        # return 0  : everything went fine, jump to next instruction.
        # return n  : everything went fine, jump to instruction n.
        # return -n : something went wrong, error code is n.
        mtd_map = {
            'incr': self.incr,
            'decr': self.decr,
            'read': self.read,
            'write': self.write,
            'load': self.load,
            'store': self.store,
            'jump': self.jump,
            'jz': self.jz,
            'stop': self.stop
        }

        if self.debug:
            print("--- (%d) ---" % self.current)
            print("Executing: %s" % " ".join(self.instructions[self.current]))
            input("Press any key to proceed...")
        elif self.verbose:
            print("%*s $ %s" % (
                len(str(len(self.instructions))), str(self.current),
                " ".join(self.instructions[self.current])
            ))

        if not self.instructions[self.current][0] in mtd_map:
            self.status = RAM_STATUS_ERROR
            self.error = RAM_ERROR_UNKNOWN
            return

        rcode = mtd_map[self.instructions[self.current][0]]()
        if rcode == 0:
            if self.current >= len(self.instructions) - 1:
                self.status = RAM_STATUS_STOP
            else:
                self.current += 1
        elif rcode > 0:
            self.current = rcode
        else:
            self.status = RAM_STATUS_ERROR
            self.error = -rcode

        if self.debug:
            print("\nRegistries: %s\n" % self.strregs())

    def run(self):
        while self.status in(RAM_STATUS_READY, RAM_STATUS_RUNNING):
            self.step()

    def strregs(self):
        reg_str = "[ A=%d ] " % self.acc
        for reg in self.regs:
            reg_str += "[ %d=%d ] " % (reg, self.regs[reg])
        return reg_str

    def strerror(self, error=None):
        if error is None:
            error = self.error

        return "Instruction #%d: %s" % (self.current, (
            "Success",
            "Empty instruction",
            "Unknown instruction",
            "Number expected",
            "Invalid register address specifier",
            "Invalid number of arguments for this instruction",
            "Invalid or non-existent target instruction",
            "Invalid input received"
        )[error])
